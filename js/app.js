'use strict';

var tetrisApp = angular.module('tetrisApp', []);

tetrisApp.factory('tetris', function() {

    var iBlock = { blocks: [0x0F00, 0x2222, 0x00F0, 0x4444], color: 'cyan'   };
    var jBlock = { blocks: [0x44C0, 0x8E00, 0x6440, 0x0E20], color: 'blue'   };
    var oBlock = { blocks: [0xCC00, 0xCC00, 0xCC00, 0xCC00], color: 'yellow' };

    var blocks = [iBlock,jBlock,oBlock];
  return new Tetris(tileNames);
});


memoryGameApp.controller('GameCtrl', function GameCtrl($scope, game) {
  $scope.game = tetris;

});
